package com.mkausa.chandacalculator;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

public class ChandaIncome extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chanda_income);

        // Go Back
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        // Annual Income hint
        final EditText editText = (EditText) findViewById(R.id.editTextAnnualIncome);
        editText.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (hasFocus) {
                    editText.setHint(getString(R.string.AnnualIncomeHint));
                } else {
                    editText.setHint("");
                }
            }
        });

        // Button click
        final Button buttonCalc = (Button) findViewById(R.id.buttonCalculate);
        buttonCalc.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {
                Intent mainIntent = new Intent(ChandaIncome.this, ChandaCalculate.class);
                ChandaIncome.this.startActivity(mainIntent);
                //MainActivity.this.finish();
            }
        });
    }
}
