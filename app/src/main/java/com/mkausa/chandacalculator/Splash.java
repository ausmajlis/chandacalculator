package com.mkausa.chandacalculator;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;

/**
 * Created by Shiraz_Laeeq on 11/29/2016.
 */

public class Splash extends Activity {

    private static int SPLASH_TIMEOUT = 2; // Seconds

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.splash);

        new Handler().postDelayed(new Runnable() {
            public void run() {

                //startActivity(new Intent(Splash.this, MainActivity.class));
                //finish();

                Intent mainIntent = new Intent(Splash.this,MainActivity.class);
                Splash.this.startActivity(mainIntent);
                Splash.this.finish();
            }
        }, SPLASH_TIMEOUT * 1000);
    }
}
