package com.mkausa.chandacalculator;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;
import android.view.View;
import android.view.View.OnClickListener;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    /** Called when the user clicks the chanda link */
    public void goToChandaIncome(View view) {
        Intent mainIntent = new Intent(MainActivity.this, ChandaIncome.class);
        MainActivity.this.startActivity(mainIntent);
        //MainActivity.this.finish();
    }

    /** Called when the user clicks the settins link */
    public void goToSettings(View view) {
        Intent mainIntent = new Intent(MainActivity.this, Settings.class);
        MainActivity.this.startActivity(mainIntent);
        //MainActivity.this.finish();
    }
}
